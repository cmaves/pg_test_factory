package pg_test_factory

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"sync"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
)

var (
	setupMap        sync.Map
	setupGlobalPool sync.Once
	globalTestPool  *pgxpool.Pool
	globalPoolErr   error
	lineReg         = regexp.MustCompile(`(?m);\s*$`)
)

type setupData struct {
	once       sync.Once
	templateId string
	dbErr      error
}

type TestingT interface {
	Errorf(format string, args ...interface{})
	FailNow()
	Helper()
	Cleanup(f func())
}

func SetupTestDb(t TestingT, pgBaseUrl string, baseName string, schema string, valFn func(*pgx.Conn) error) *pgxpool.Pool {
	t.Helper()
	ctx := context.Background()

	setupGlobalPool.Do(func() {
		var (
			testDbUrl  = pgBaseUrl + "postgres"
			poolConfig *pgxpool.Config
		)

		if poolConfig, globalPoolErr = pgxpool.ParseConfig(testDbUrl); globalPoolErr != nil {
			return
		}

		globalTestPool, globalPoolErr = pgxpool.ConnectConfig(ctx, poolConfig)
		return
	})
	require.Nil(t, globalPoolErr)

	value, _ := setupMap.LoadOrStore(baseName, &setupData{})
	data := value.(*setupData)

	data.once.Do(func() {
		// create template Database
		localTemplate := fmt.Sprintf("%s_%x", baseName, rand.Uint64())
		query := fmt.Sprint("CREATE DATABASE ", localTemplate)
		if _, data.dbErr = globalTestPool.Exec(ctx, query); data.dbErr != nil {
			return
		}
		data.templateId = localTemplate

		// connect to the template database
		templateDbUrl := pgBaseUrl + localTemplate
		var config *pgx.ConnConfig
		if config, data.dbErr = pgx.ParseConfig(templateDbUrl); data.dbErr != nil {
			return
		}
		var pg *pgx.Conn
		if pg, data.dbErr = pgx.ConnectConfig(ctx, config); data.dbErr != nil {
			return
		}
		defer pg.Close(ctx)

		// build the template database
		lines := lineReg.Split(schema, -1)
		data.dbErr = pg.BeginTxFunc(ctx, pgx.TxOptions{IsoLevel: pgx.Serializable}, func(t pgx.Tx) error {
			for _, line := range lines {
				if line == "" {
					continue
				}
				if _, err := t.Exec(ctx, line); err != nil {
					return err
				}
			}
			return nil
		})
		if data.dbErr != nil {
			return
		}
		if valFn != nil {
			require.Nil(t, valFn(pg))
		}
	})

	require.Nil(t, data.dbErr)

	// create the test-specific database from the template
	dbName := fmt.Sprintf("%s_%x", baseName, rand.Uint32())
	createQuery := fmt.Sprintf("CREATE DATABASE %s TEMPLATE %s", dbName, data.templateId)
	_, err := globalTestPool.Exec(ctx, createQuery)
	require.Nil(t, err)

	// setup the cleanup for the test DB
	t.Cleanup(func() {
		deleteQuery := "DROP DATABASE " + dbName
		globalTestPool.Exec(ctx, deleteQuery)
	})

	// connect to the test specific database
	dbUrl := pgBaseUrl + dbName
	poolConfig, err := pgxpool.ParseConfig(dbUrl)
	require.Nil(t, err)

	// setup configurations
	poolConfig.MaxConns = 5
	poolConfig.MaxConnIdleTime = 5 * time.Minute
	poolConfig.MaxConnLifetime = 5 * time.Minute
	poolConfig.ConnConfig.PreferSimpleProtocol = true
	poolConfig.ConnConfig.BuildStatementCache = nil

	pool, err := pgxpool.ConnectConfig(ctx, poolConfig)
	require.Nil(t, err)

	// setup clean up for the pool
	t.Cleanup(pool.Close)

	return pool
}

func CleanupDBs(logErrors bool) error {
	if globalTestPool == nil {
		return nil
	}
	defer globalTestPool.Close()

	var (
		wg     sync.WaitGroup
		errMap sync.Map
	)
	setupMap.Range(func(key, value interface{}) bool {
		data := value.(*setupData)
		if data.templateId == "" {
			return true
		}
		wg.Add(1)
		go func() {
			ctx, cancelFn := context.WithTimeout(context.Background(), time.Second*15)
			// clean up the test template database
			defer cancelFn()

			deleteQuery := "DROP DATABASE " + data.templateId
			if _, dbErr := globalTestPool.Exec(ctx, deleteQuery); dbErr != nil {
				errMap.Store(key, dbErr)
			}
			wg.Done()
		}()
		return true
	})

	wg.Wait() // wait until all DBs are cleaned up

	if logErrors {
		errMap.Range(func(key, value interface{}) bool {
			fmt.Fprintf(os.Stderr, "Failed to templdate DB %s: %v\n", key, value)
			return true
		})
	}

	var firstErr error
	errMap.Range(func(_, err interface{}) bool {
		firstErr = err.(error)
		return false // unreachable
	})
	return firstErr
}
