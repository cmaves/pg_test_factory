package pg_test_factory

import (
	// "os"

	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const testBaseUrl = "postgres://fake:password@postgres:5432/"

func TestDbSetup(t *testing.T) {
	const goodSchema = "CREATE TABLE hello (uuid uuid, PRIMARY KEY(uuid));"
	ctx := context.Background()
	t.Cleanup(func() {
		globalPoolErr = nil
		globalTestPool = nil
		setupMap.Range(func(key, _ interface{}) bool {
			setupMap.Delete(key)
			return true
		})
	})

	rand.Seed(time.Now().UnixMilli())

	shouldFailTests := map[string]struct {
		shouldFailBody func(t TestingT)
	}{
		"InvalidSchema": {
			shouldFailBody: func(t TestingT) {
				SetupTestDb(t, testBaseUrl, "fake_bad_schema", "INVALID COMMAND;", nil)
			},
		},
		"ValFnFails": {
			shouldFailBody: func(t TestingT) {
				SetupTestDb(t, testBaseUrl, "fake_bad_val", goodSchema, func(_ *pgx.Conn) error { return errors.New("Validation error") })
			},
		},
	}
	t.Run("SetupGroups", func(t *testing.T) {
		t.Run("ValidSchema", func(t *testing.T) {
			t.Parallel()
			pg := SetupTestDb(t, testBaseUrl, "fake_good_schema", goodSchema, nil)
			require.NotNil(t, pg)

			row := pg.QueryRow(ctx, "SELECT 'hello' IN (SELECT tablename FROM pg_catalog.pg_tables)")
			var found bool
			err := row.Scan(&found)
			require.Nil(t, err)
			assert.True(t, found)
		})

		for name, tc := range shouldFailTests {
			testCase := tc
			t.Run(name, func(t *testing.T) {
				t.Parallel()
				ft := failTracker{t: t}
				defer func() {
					r := recover()
					assert.Equal(t, failTrackerFail, r)
				}()
				testCase.shouldFailBody(ft)
			})
		}
	})

	require.NotNil(t, globalTestPool)

	conn, err := pgx.Connect(ctx, testBaseUrl+"postgres")
	require.Nil(t, err)
	defer conn.Close(ctx)

	badNames := []string{"fake_bad_val", "fake_bad_schema"}
	center := strings.Join(badNames, "', '")
	queryStr := fmt.Sprintf(`SELECT EXISTS (SELECT * FROM pg_database WHERE datname IN ('%s'))`, center)
	row := conn.QueryRow(ctx, queryStr)
	var found bool
	err = row.Scan(&found)
	require.Nil(t, err)
	assert.False(t, found)

	err = CleanupDBs(true)
	require.Nil(t, err)

	names := append(badNames, "fake_good_schema")
	center = strings.Join(names, "', '")
	queryStr = fmt.Sprintf(`SELECT EXISTS (SELECT * FROM pg_database WHERE datname IN ('%s'))`, center)
	row = conn.QueryRow(ctx, queryStr)
	err = row.Scan(&found)
	require.Nil(t, err)
	assert.False(t, found)

}

type failTracker struct {
	failed bool
	t      *testing.T
}

func (t failTracker) Cleanup(f func()) {
	t.t.Cleanup(f)
}

var failTrackerFail = errors.New("fail tracker fail err")

func (t failTracker) Errorf(format string, args ...interface{}) {
	t.failed = true
	panic(failTrackerFail)
}

func (t failTracker) FailNow() {
	t.failed = true
	panic(failTrackerFail)
}

func (t failTracker) Helper() {
	// does nothing
}

/*func TestMain(m *testing.M) {
	code := m.Run()
	err := CleanupDBs(true)
    if err != nil && code == 0 {
        os.Exit(123)
    }
	os.Exit(code)
}*/
